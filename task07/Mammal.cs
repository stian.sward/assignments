﻿using task05;

namespace task07
{
    class Mammal : Animal
    {
        public int PregnancyMonths { get; set; }

        public Mammal(string name, string kind, double height, double weight, int pregnancyMonths) : base(name, kind, height, weight)
        {
            PregnancyMonths = pregnancyMonths;
        }

        public override string Summary() => Name + " is a " + Kind + ", which is a type of mammal. The females are usually pregnant for " + PregnancyMonths + " months.";
    }
}
