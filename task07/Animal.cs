﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task05
{
    abstract class Animal
    {
        public string Name { get; set; }
        public string Kind { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }

        public Animal()
        {
            Name = "Roger";
            Kind = "Rabbit";
            Height = 0.25;
            Weight = 3.0;
        }

        public Animal(string name, string kind, double height, double weight)
        {
            Name = name;
            Kind = kind;
            Height = height;
            Weight = weight;
        }


        public abstract string Summary();

        public override string ToString()
        {
            return Name + " the " + Kind;
        }
    }
}
