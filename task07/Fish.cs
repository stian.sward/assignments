﻿using System;
using System.Collections.Generic;
using System.Text;
using task05;

namespace task07
{
    class Fish : Animal
    {
        public string WaterType { get; set; }

        public Fish(string name, string kind, double height, double weight, string waterType) : base(name, kind, height, weight)
        {
            WaterType = waterType;
        }

        public override string Summary() => Name + " is a " + Kind + ", which is a type of fish that swims in " + WaterType + ".";
    }
}
