﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using task05;

namespace task07
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>
            {
                new Mammal("Roger", "rabbit", 0.25, 3.0, 3),
                new Fish("Hans", "trout", 0.12, 3.0, "salt water")
            };

            foreach (Animal a in animals)
            {
                Console.WriteLine(a.Summary());
            }
        }
    }
}
