﻿using System;
using System.Collections.Generic;

namespace task09
{
    class ChaosArray<T>
    {
        T[] array = new T[10];
        Random rnd = new Random();

        public int Insert(T obj)
        {
            int index = rnd.Next(10);
            if (array[index] is T)
            {
                throw new UnavailableSpaceException("No available space at index " + index);
            }
            else
            {
                array[index] = obj;
                return index;
            }
        }
        public T Retrieve()
        {
            int index = rnd.Next(10);
            if (array[index] is null)
            {
                throw new ItemNotFoundException("No item exists at index " + index);
            }
            else
                return array[index];
        }
    }
}
