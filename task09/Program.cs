﻿using System;

namespace task09
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> arr = new ChaosArray<int>();

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Console.WriteLine("Inserted " + i + " at index " + arr.Insert(i));
                } catch (UnavailableSpaceException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Console.WriteLine(arr.Retrieve());
                } catch (ItemNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
