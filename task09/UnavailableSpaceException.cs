﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task09
{
    class UnavailableSpaceException : Exception
    {
        public UnavailableSpaceException() { }

        public UnavailableSpaceException(string message)
            : base(message) { }

        public UnavailableSpaceException(string message, Exception inner)
            : base(message, inner) { }
    }
}
