﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task09
{
    class ItemNotFoundException : Exception
    {
        public ItemNotFoundException() { }

        public ItemNotFoundException(string message)
            : base(message) { }

        public ItemNotFoundException(string message, Exception inner)
            : base(message, inner) { }
    }
}
