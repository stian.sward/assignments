﻿using System;
using System.Collections.Generic;

namespace task04
{
    class Task03
    {
        static void Main(string[] args)
        {
            // Create sample phonebook
            List<Tuple<string, string>> phonebook = CreateSamplePhonebook();

            // Print phonebook
            Console.WriteLine("Initial phonebook:");
            PrintList(phonebook);

            Console.Write("\nEnter search term: ");
            string query = Console.ReadLine();

            // Print search result
            PrintList(Search(phonebook, query));
        }

        /**
         * Creates a sample phonebook with some default names
         */
        static List<Tuple<string, string>> CreateSamplePhonebook()
        {
            List<Tuple<string, string>> phonebook = new List<Tuple<string, string>>
            {
                new Tuple<string, string>("John", "Doe"),
                new Tuple<string, string>("Jane", "Doe"),
                new Tuple<string, string>("Max", "Mekker"),
                new Tuple<string, string>("Mikel", "Arteta"),
                new Tuple<string, string>("Magnus", "Maximus")
            };

            return phonebook;
        }

        /**
         * Searches for a given query among the given list of Tuples, adding every match to a list
         * 
         * @param   lst     The search space
         * @param   query   The string to match with
         * 
         * @returns res     List of matches
         */
        static List<Tuple<string, string>> Search(List<Tuple<string, string>> lst, string query)
        {
            List<Tuple<string, string>> res = new List<Tuple<string, string>>();

            foreach (Tuple<string, string> contact in lst)
            {
                if (contact.Item1.ToLower().Contains(query.ToLower()) || contact.Item2.ToLower().Contains(query.ToLower()))
                {
                    res.Add(contact);
                }
            }

            return res;
        }

        /**
         * Prints a given list by calling each object's ToString-method
         * 
         * @param   lst     List of objects to be printed
         */
        static void PrintList(List<Tuple<string, string>> lst)
        {
            if (lst.Count == 0)
            {
                Console.WriteLine("No results.");
            }
            foreach (Tuple<string, string> name in lst)
            {
                Console.WriteLine(name.ToString());
            }
        }
    }
}
