﻿using System;
using System.Linq;

namespace task02
{
    class Task02
    {
        static void Main(string[] args)
        {
            // Prompt and collect user input
            Console.Write("Enter the horizontal width: ");
            int horSize = GetInt();
            Console.Write("Enter the vertical height: ");
            int verSize = GetInt();
            Console.Write("Choose a character: ");
            string sym = Console.ReadLine()[0] + " ";

            // Draw rectangle
            DrawRectangle(horSize, verSize, sym);
        }

        /**
         * Draws the rectangle given by the width, height and symbol choice
         * 
         * @param   horSize     The width
         * @param   verSize     The height
         * @param   sym         The symbol to draw with
         */
        static void DrawRectangle(int horSize, int verSize, string sym)
        {
            // Draw the top line
            Console.WriteLine(String.Concat(Enumerable.Repeat(sym, horSize)));

            // Draw the sides
            string line = sym + String.Concat(Enumerable.Repeat("  ", horSize - 2)) + sym + "\n";
            Console.Write(String.Concat(Enumerable.Repeat(line, verSize - 2)));

            // Draw the bottom line
            Console.WriteLine(String.Concat(Enumerable.Repeat(sym, horSize)));
        }

        /**
         * Parses console input to integer
         * 
         * @returns     int size    The parsed integer value
         */
        static int GetInt()
        {
            int size = 0;
            do
            {
                try
                {
                    size = int.Parse(Console.ReadLine());
                    if (size < 2)
                    {
                        Console.WriteLine("Size must be at least 2");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            } while (size < 2);
            return size;
        }
    }
}
