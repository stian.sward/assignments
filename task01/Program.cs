﻿using System;

namespace assignment00
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Hello {0}, your name is {1} characters long and starts with a {2}", name, name.Length, name[0]);
        }
    }
}
