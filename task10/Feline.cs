﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task10
{
    class Feline : Mammal, IRunner
    {
        public int TopSpeedKmpH { get; set; }
        public Feline(string name, string subSpecies, double height, double weight, double furLength, int topSpeed) 
            : base(name, subSpecies, height, weight, furLength)
        {
            TopSpeedKmpH = topSpeed;
        }

        public void Sprint(int distance)
        {
            Console.WriteLine(Name + " ran " + distance + "m in " + (distance / TopSpeedKmpH) / 3.6 + "s");
        }

        public override string Summary()
        {
            return base.Summary() + " They can run at up to " + TopSpeedKmpH + "km/h";
        }
    }
}
