﻿namespace task10
{
    class Mammal : Animal
    {
        public double FurLength { get; set; }

        public Mammal(string name, string subSpecies, double height, double weight, double furLength) : base(name, subSpecies, height, weight)
        {
            FurLength = furLength;
        }

        public override string Summary()
        {
            return ToString() + " is a type of " + GetType().Name + ". Their fur is " + FurLength + "cm long.";
        }
    }
}
