﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace task10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>
            {
                new Mammal("Roger", "rabbit", 40, 2.0, 2.5),
                new Fish("Hans", "trout", 12, 3.0, "saltwater"),
                new Bird("Peter", "parrot", 25, 0.8, 50),
                new Feline("Charlie", "cheetah", 110, 50, 3.0, 110),
                new Bird("Chris", "crow", 15, 0.4, 40),
                new Feline("Perry", "panther", 120, 60, 3.0, 70)
            };

            Console.Write("Press enter to use default list, or write anything to add your own animals: ");
            if (Console.ReadLine() != "")
            {
                animals = AddAnimals();
            } else
            {
                Console.WriteLine("Using default list:");
                PrintAnimals(animals);
            }

            PrintAnimals(Filter(animals));
        }

        static IEnumerable<Animal> Filter(IEnumerable<Animal> animals)
        {
            string nameFilter = "", input;
            double heightFilter = default(double), weightFilter = default(double);

            Console.Write("If you want to filter on name, write your query here, otherwise leave blank: ");
            nameFilter = Console.ReadLine();
            Console.Write("If you want to filter on height enter here or leave blank: ");
            input = Console.ReadLine();
            if (input != "") heightFilter = double.Parse(input);
            Console.Write("If you want to filter on weight enter here or leave blank: ");
            input = Console.ReadLine();
            if (input != "") weightFilter = double.Parse(input);

            return SubSet(animals, nameFilter, heightFilter, weightFilter);
        }

        static IEnumerable<Animal> SubSet(IEnumerable<Animal> animals, string nameFilter, double heightFilter, double weightFilter)
        {
            IEnumerable<Animal> res = animals.ToList();
            if (!(nameFilter == ""))
            {
                Console.WriteLine("nameFilter");
                res = from animal in res
                      where animal.Name.Contains(nameFilter, StringComparison.InvariantCultureIgnoreCase)
                      orderby animal.Name
                      select animal;
            }
            if (!heightFilter.Equals(default(double)))
            {
                Console.WriteLine("heightFilter");
                res = from animal in res
                      where animal.Height == heightFilter
                      select animal;
            }
            if (!weightFilter.Equals(default(double)))
            {
                Console.WriteLine("weightFilter");
                res = from animal in res
                      where animal.Weight == weightFilter
                      select animal;
            }
            return res;
        }

        static void PrintAnimals(IEnumerable<Animal> animals)
        {
            if (animals.Count() == 0)
            {
                Console.WriteLine("List is empty.");
            } else
            {
                foreach (Animal animal in animals)
                {
                    Console.WriteLine(animal);
                }
            }
        }

        static List<Animal> AddAnimals()
        {
            Console.WriteLine("Let's add some animals to the list.\nThe available species are:\n1. Bird\n2. Feline\n3. Fish\n4. Mammal");
            string species = "", name = "", subSpecies = "", waterType = "";
            double height, weight, furLength, wingspan;
            int topSpeed;
            List<Animal> ret = new List<Animal>();
            while (true)
            {
                Console.Write("Choose one of the numbers, or leave blank to stop adding animals: ");
                species = Console.ReadLine();
                if (species == "") break;

                Console.Write("Name: ");
                name = Console.ReadLine();
                Console.Write("Subspecies: ");
                subSpecies = Console.ReadLine();
                Console.Write("Height (cm): ");
                height = double.Parse(Console.ReadLine());
                Console.Write("Weight (kg): ");
                weight = double.Parse(Console.ReadLine());
                switch (species)
                {
                    case "1":
                        Console.Write("Wingspan: ");
                        wingspan = double.Parse(Console.ReadLine());
                        ret.Add(new Bird(name, subSpecies, height, weight, wingspan));
                        break;
                    case "2":
                        Console.Write("Fur length (cm): ");
                        furLength = double.Parse(Console.ReadLine());
                        Console.Write("Top speed (km/h): ");
                        topSpeed = int.Parse(Console.ReadLine());
                        ret.Add(new Feline(name, subSpecies, height, weight, furLength, topSpeed));
                        break;
                    case "3":
                        Console.Write("Water type: ");
                        waterType = Console.ReadLine();
                        ret.Add(new Fish(name, subSpecies, height, weight, waterType));
                        break;
                    case "4":
                        Console.Write("Fur length (cm): ");
                        furLength = double.Parse(Console.ReadLine());
                        ret.Add(new Mammal(name, subSpecies, height, weight, furLength));
                        break;
                    default:
                        Console.WriteLine("That species has not been implemented. Please select one from the list.");
                        break;
                }
            }

            Console.WriteLine("Your custom list:");
            PrintAnimals(ret);
            return ret;
        }
    }
}
