﻿namespace task10
{
    abstract class Animal
    {
        public string Name { get; set; }
        public string SubSpecies { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }

        public Animal()
        {
            Name = "Roger";
            SubSpecies = "Rabbit";
            Height = 0.25;
            Weight = 3.0;
        }

        public Animal(string name, string subSpecies, double height, double weight)
        {
            Name = name;
            SubSpecies = subSpecies;
            Height = height;
            Weight = weight;
        }


        public abstract string Summary();

        public override string ToString()
        {
            return Name + " the " + SubSpecies;
        }
    }
}
