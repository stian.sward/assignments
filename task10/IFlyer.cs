﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task10
{
    interface IFlyer
    {
        double Wingspan { get; set; }
        public void Fly(double distance, string direction);
    }
}
