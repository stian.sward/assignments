﻿namespace task10
{
    class Fish : Animal, ISwimmer
    {
        public string WaterType { get; set; }

        public Fish(string name, string subSpecies, double height, double weight, string waterType) : base(name, subSpecies, height, weight)
        {
            WaterType = waterType;
        }

        public override string Summary()
        {
            return ToString() + " is a type of " + GetType().Name + " that swims in " + WaterType + ".";
        }

        public void Swim()
        {
            System.Console.WriteLine("Blub blub");
        }
    }
}
