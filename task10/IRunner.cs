﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task10
{
    interface IRunner
    {
        public int TopSpeedKmpH { get; set; }
        public void Sprint(int distance);
    }
}
