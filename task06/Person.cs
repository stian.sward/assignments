﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace task06
{
    class Person : IComparable<Person>
    {
        private string firstName;
        private string lastName;
        private int phoneNo;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNo { get; set; }

        public Person()
        {
            firstName = "Jane";
            lastName = "Doe";
            phoneNo = 81549300;
        }

        public Person(string firstName, string lastName, int phoneNo)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNo = phoneNo;
        }

        public bool Contains(string query) => firstName.ToLower().Contains(query.ToLower()) || lastName.ToLower().Contains(query.ToLower());

        public override string ToString() => lastName + ", " + firstName + ": " + phoneNo;

        public int CompareTo(Person other)
        {
            int res = lastName.CompareTo(other.lastName);
            return (res == 0) ? firstName.CompareTo(other.firstName) : res;
        }
    }
}
