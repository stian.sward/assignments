﻿using System;
using System.Collections.Generic;

namespace task06
{
    class Task06
    {
        static void Main(string[] args)
        {
            List<Person> phonebook = SamplePhonebook;

            phonebook.Sort();

            Console.WriteLine("Initial phonebook (sorted):");
            Print(phonebook);

            Console.Write("Enter search term: ");
            List<Person> searchRes = Search(phonebook, Console.ReadLine());

            if (searchRes.Count == 0)
            {
                Console.WriteLine("\nNo matches found.");
            }
            else
            {
                Console.WriteLine("\nSearch results:");
                Print(searchRes);
            }
        }

        static List<Person> SamplePhonebook
        {
            get
            {
                List<Person> lists = new List<Person>
            {
                new Person(),
                new Person("John", "Doe", 12345678),
                new Person("Max", "Mekker", 23456789),
                new Person("Mikel", "Arteta", 45464748),
                new Person("Magnus", "Maximus", 98979695),
            };
                return lists;
            }
        }

        static List<Person> Search(List<Person> list, string query)
        {
            List<Person> res = new List<Person>();
            foreach (Person p in list)
            {
                if (p.Contains(query))
                {
                    res.Add(p);
                }
            }
            return res;
        }

        static void Print(List<Person> list)
        {
            foreach (Person p in list)
            {
                Console.WriteLine(p.ToString());
            }
        }
    }
}
