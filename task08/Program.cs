﻿using System;
using System.Collections.Generic;

namespace task08
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>
            {
                new Mammal("Roger", "rabbit", 0.4, 2.0, 2.5),
                new Fish("Hans", "trout", 12, 3.0, "saltwater"),
                new Bird("Peter", "parrot", 0.25, 0.8, 50),
                new Feline("Charlie", "cheetah", 1.1, 50, 3.0, 110)
            };

            foreach (Animal a in animals)
            {
                Console.WriteLine(a.Summary());
            }
        }
    }
}
