﻿namespace task08
{
    class Mammal : Animal
    {
        public double FurLength { get; set; }

        public Mammal(string name, string kind, double height, double weight, double furLength) : base(name, kind, height, weight)
        {
            FurLength = furLength;
        }

        public override string Summary()
        {
            return ToString() + " is a type of " + GetType().Name + ". Their fur is " + FurLength + "cm long.";
        }
    }
}
