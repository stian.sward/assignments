﻿using System;

namespace task08
{
    class Bird : Animal, IFlyer
    {
        public double Wingspan { get; set; }

        public Bird(string name, string species, double height, double weight, double wingspan) : base(name, species, height, weight)
        {
            Wingspan = wingspan;
        }

        public void Fly(double distance, string direction)
        {
            Console.WriteLine("{Name} flew {distance}km to the {direction}");
        }

        public override string Summary()
        {
            return ToString() + " is a type of " + GetType().Name + ". They have a " + Wingspan + "m wingspan.";
        }
    }
}
