﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task08
{
    interface IRunner
    {
        public int TopSpeedKmpH { get; set; }
        public void Sprint(int distance);
    }
}
