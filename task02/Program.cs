﻿using System;
using System.Linq;

namespace task02
{
    class Program
    {
        static void Main(string[] args)
        {
            // Prompt and collect user input
            Console.Write("Enter the horizontal width: ");
            int horSize = GetSize();
            Console.Write("Enter the vertical height: ");
            int verSize = GetSize();
            Console.Write("Choose a character: ");
            string sym = Console.ReadLine()[0] + " ";

            // Draw rectangle
            DrawTop(horSize, sym);
            DrawSides(horSize, verSize, sym);
            DrawTop(horSize, sym);
        }

        /**
         * Draw the top or bottom edge
         * 
         * @param   int size    The width of the line
         * @param   string sym  The symbol to draw
         */
        static void DrawTop(int size, string sym)
        {
            Console.WriteLine(String.Concat(Enumerable.Repeat(sym, size)));
        }

        /**
         * Draw the middle lines filled with spaces
         * 
         * @param   int horSize     Total width of the square
         * @param   int verSize     Total height of the square
         * @param   string sym      The symbol to draw
         */
        static void DrawSides(int horSize, int verSize, string sym)
        {
            string line = sym + String.Concat(Enumerable.Repeat("  ", horSize - 2)) + sym;
            for (int i = 0; i < verSize - 2; i++)
            {
                Console.WriteLine(line);
            }
        }

        /**
         * Parses console input to integer
         * 
         * @returns     int size    The parsed integer value
         */
        static int GetSize()
        {
            int size = 0;
            do
            {
                try
                {
                    size = int.Parse(Console.ReadLine());
                    if (size < 2)
                    {
                        Console.WriteLine("Size must be at least 2");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            } while (size < 2);
            return size;
        }
    }
}
