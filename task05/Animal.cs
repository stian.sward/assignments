﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task05
{
    class Animal
    {
        private string name;
        private string kind;
        private double height;
        private double weight;
        public string Name { get; set; }
        public string Kind { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }

        public Animal()
        {
            name = "Roger";
            kind = "Rabbit";
            height = 0.25;
            weight = 3.0;
        }

        public Animal(string name, string kind, double height, double weight)
        {
            this.name = name;
            this.kind = kind;
            this.height = height;
            this.weight = weight;
        }

        public void Move(string direction)
        {
            Console.WriteLine(name + " moved " + direction);
        }

        public string Summary()
        {
            return name + " is a " + kind + ". They are " + height + "m tall and weigh " + weight + "kg.";
        }

        public override string ToString()
        {
            return name + " the " + kind;
        }
    }
}
