﻿using System;
using System.Collections.Generic;

namespace task05
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal());
            animals.Add(new Animal("Fenton", "Dog", 0.9, 35.0));
            animals.Add(new Animal("Sir Meowington", "Cat", 0.4, 8.5));

            foreach (Animal a in animals)
            {
                Console.WriteLine(a.Summary());
            }
        }
    }
}
